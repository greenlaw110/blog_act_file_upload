#How to handle File Upload in Actframework

This project demonstrates how to handle file upload in [Actframework](http://actframework.org)

To run this app, type the following command in the project folder:

```bash
mvn clean compile exec:exec
```

Once the app started, you can open your browser and navigate to `http://localhost:5460` to play with it