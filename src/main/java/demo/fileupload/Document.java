package demo.fileupload;

import act.util.SimpleBean;
import org.osgl.util.S;

import java.io.File;

public class Document implements SimpleBean {
    public String desc;
    public String subject;
    public File attachment;

    public Document(String subject, String desc, File attachment) {
        this.desc = desc;
        this.subject = subject;
        this.attachment = attachment;
    }

    public FileInfo getAttachment() {
        return new FileInfo(attachment);
    }
}
