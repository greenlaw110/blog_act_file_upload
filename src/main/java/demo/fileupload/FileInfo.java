package demo.fileupload;

import act.util.SimpleBean;

import java.io.File;

public class FileInfo implements SimpleBean {
    public String name;
    public long size;

    public FileInfo(File file) {
        this.name = file.getName();
        this.size = file.length();
    }
}
