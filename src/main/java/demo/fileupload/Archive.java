package demo.fileupload;

import act.util.SimpleBean;
import org.osgl.util.C;
import org.osgl.util.S;

import java.io.File;
import java.util.List;

public class Archive implements SimpleBean {
    public String desc;
    public String subject;
    public File[] attachments;

    public Archive(String subject, String desc, File[] attachments) {
        this.desc = desc;
        this.subject = subject;
        this.attachments = attachments;
    }

    public List<FileInfo> getAttachments() {
        return C.listOf(attachments).map(FileInfo::new);
    }
}
