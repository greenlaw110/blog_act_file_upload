package demo.fileupload;

import act.Act;
import act.Version;
import org.osgl.mvc.annotation.GetAction;
import org.osgl.mvc.annotation.PostAction;

import java.io.File;

public class AppEntry {

    @GetAction
    public void home() {}

    @PostAction("/single")
    public Document handleSingleFile(File file, String subject, String desc) {
        return new Document(subject, desc, file);
    }

    @PostAction("/multi")
    public Archive handleMultipleFiles(File[] files, String subject, String desc) {
        return new Archive(subject, desc, files);
    }

    public static void main(String[] args) throws Exception {
        Act.start("File Upload Demo", Version.appVersion(), AppEntry.class);
    }

}
